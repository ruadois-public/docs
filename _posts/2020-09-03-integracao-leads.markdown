---
layout: post
title: Integração de Leads
date: 2020-09-03
---

## Endpoint

`POST api.ruadois.com.br/api/v1/leads/external/:ID_DA_IMOBILIARIA`

### Autenticação

A chave de autenticação e o `ID_DA_IMOBILIARIA` devem ser **solicitados** para o Suporte Técnico da Rua Dois.
Com a chave em mãos, ela deve ser passada no `Header` da requisição, por meio do campo
`Authorization`, como no exemplo:

```
Authorization: Basic SUA_CHAVE_DE_AUTENTICACAO_AQUI
```

### Body

```json
{
    "origin": "algumPortal",
    "originId": "626262",
    "originAdCode": "1098765",
    "originCreatedAt": "2020-06-18T19:43:11.061Z",
    "campaign": "blackFriday",
    "message": "Gostaria de visitar o imóvel.",
    "messenger": "whatsapp",
    "propertyCode": "R20002",
    "profile": {
        "name": "Miguel",
	"email": "miguel@lead.com",
        "phone": "+5511999999999"
    }
}
```

- **origin**: identificador do portal que originou o lead.
- **originId**: identificador do lead no portal de origem.
- **originAdCode**: identificador do anúncio no portal que gerou o lead.
- **originCreatedAt**: data de criação do lead no portal de origem.
- **campaign**: identificador da campanha em vigor.
- **message**: mensagem do cliente.
- **messenger**: meio de comunicação pelo qual o lead foi gerado.
- **propertyCode**: código do imóvel informado pela imobiliária para registro do imóvel na RuaDois.
- **profile.name**: Nome do cliente que gerou o lead.
- **profile.email** email do cliente que gerou o lead.
- **profile.phone** telefone do cliente que gerou o lead.

#### Campos obrigatórios

- `propertyCode`
- Pelo menos um dentre os campos `email` e `phone` deve ser informado.

## API de Homologação

Testes podem ser realizados por meio do endpoint:

`POST hmg.ruadois.com.br/api/v1/leads/external/42`

Passando no `Header` a chave:

```
Authorization: Basic dGVzdDpYNGtUU2s1ZTBaWlZRWlo2RUhzZw==
```

E o `propertyCode: R20005`, referente ao imóvel disponibilizado para testes.
