---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: post
title: Integração CRMs e Plataforma RuaDois
date: 2020-07-14
---

A integração entre os CRMs das imobiliárias e a Plataforma da RuaDois acontece por meio de um arquivo XML, semelhante aos que são enviados para outras plataformas de divulgação dos imóveis.

## O que é o XML?

A sigla XML significa eXtensible Markup Language e refere-se a um tipo de arquivo estruturado que facilita o compartilhamento de informações entre diferentes sistemas. Ele é caracterizado por Tags que identificam alguma informação, como no exemplo abaixo, onde podemos identificar facilmente qual é código do imóvel e em qual endereço está localizado:

```xml
<Imovel>
  <Codigo>R2-001</Codigo>
  <Endereco>Rua das Palmeiras, Taguatinga - DF</Endereco>
</Imovel>
```
---

## Como funciona a importação do XML para a plataforma RuaDois?

A imobiliária deve disponiblizar um link para download do arquivo XML. Este arquivo deve conter uma lista de imóveis que devem ser importados com suas respectivas informações. O link fica salvo no nosso sistema, e toda noite, automaticamente, nós baixamos o arquivo, pegamos os imóveis informados e registramos na Plataforma RuaDois. As etapas para a importação de um imóvel, contemplam as duas seguintes situações:

### 1º caso: Imóvel novo na plataforma
Quando o imóvel é novo na Plataforma RuaDois ele passa pelas seguintes verificações antes de ser cadastrado no sistema:

1. Verifica-se ele possui as seguintes **informações mínimas**:
  - Código do imóvel registrado no CRM
  - Tipo do imóvel (apartamento, casa, galpão, etc)
  - CEP
  - Bairro
  - Cidade
  - UF
  - Logradouro
  - Número do Lote
  - Preço de aluguel
  - Valor do IPTU
1. **Verifica-se se o tipo de imóvel deve ser importado**. No cadastro da imobiliária, ela pode dizer se deseja utilizar a Plataforma RuaDois para trabalhar somente com casas, ou com apartamentos, ou com outros tipos de imóveis. E com base na preferência da imobiliária, nós só importamos um imóvel para o nosso sistema se estiver na lista de tipos de imóveis que ela deseja atuar.
1. Verifica-se se o bairro em que o imóvel se localiza pertence aos bairros de atuação da imobiliária com a Plataforma RuaDois. No cadastro da imobiliária é informado em quais bairros ela deseja atuar com o sistema da RuaDois, e com base nessa preferência, **nós só importamos os imóveis que pertencem a esses bairros**.

### 2º caso: Imóvel já cadastrado na plataforma
Sempre antes de importar um imóvel, o sistema verifica, pelo código presente no XML, se o respectivo imóvel já está cadastrado no sistema. Em caso afirmativo o sistema atualiza somente as informações fincanceiras, fotos e outros dados não são atualizados.

Neste caso de imóveis já cadastrados, podem ocorrer três situações:

1. Imóvel publicado na Plataforma RuaDois e presente no XML: segue o fluxo padrão atualizando os dados financeiros.
1. Imóvel despublicado na Plataforma RuaDois e presente no XML: o imóvel é republicado novamente na Plataforma RuaDois. Entendemos que, sempre que um imóvel é enviado pelo XML a imobiliária tem a intenção de publicar e atuar com o nosso sistema sobre esse imóvel.
1. Imóvel publicado na Plataforma RuaDois e removido do XML: o imóvel é despublicado. Se o imóvel não consta mais presente no XML, entendemos que a imobiliária não deseja mais divulgar e atuar sobre ele, seja porque ele foi alugado ou por qualquer outro motivo, então desativamos ele na plataforma.

---

## Estrutura do XML

Abaixo está um exemplo de XML válido que pode ser importado pela Plataforma RuaDois. <a href="https://r2service-homologation.s3-sa-east-1.amazonaws.com/demo.nidoimovel.xml" target="_blank">Neste link</a> também é possível baixar um XML de exemplo.

```xml
<Carga xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
<Imoveis>
  <Imovel>
    <CodigoImovel>R20041</CodigoImovel>
    <Endereco>Quadra 503</Endereco>
    <Complemento>Conjunto F</Complemento>
    <Bairro>Taguatinga Norte</Bairro>
    <Cidade>Brasília</Cidade>
    <Descricao>
        Não perca as oportunidades incríveis que preparamos para você. Na Black Friday de Imóveis RuaDois você encontra descontos exclusivos e imperdíveis na compra ou aluguel do seu imóvel. Aqui as suas economias valem muito mais!
     </Descricao>
    <Titulo>Residencial RuaDois - 1 Quarto Armários Vaga</Titulo>
    <CEP>72588391</CEP>
    <Numero>302</Numero>
    <Andar>3</Andar>
    <UF>DF</UF>
    <AreaTotal>0</AreaTotal>
    <AreaUtil>30</AreaUtil>
    <PrecoCondominio>130,00</PrecoCondominio>
    <PrecoLocacao>900,00</PrecoLocacao>
    <PrecoLocacaoTemporada>0,00</PrecoLocacaoTemporada>
    <PrecoVenda>110.000,00</PrecoVenda>
    <QtdBanheiros>1</QtdBanheiros>
    <QtdDormitorios>1</QtdDormitorios>
    <QtdSalas>1</QtdSalas>
    <QtdSuites>1</QtdSuites>
    <QtdVagas>1</QtdVagas>
    <Iptu>0</Iptu>
    <ValorMensal>900</ValorMensal>
    <Latitude>-17.000000</Latitude>
    <Longitude>-50.000000</Longitude>
    <TipoImovel>Apartamento</TipoImovel>
    <SubTipoImovel>Apartamento Padrão</SubTipoImovel>
    <CategoriaImovel>Padrão</CategoriaImovel>
    <Fogao>1</Fogao>
    <Mobiliado>1</Mobiliado>
    <PermiteAnimais>1</PermiteAnimais>
    <Fotos>
      <Foto>
        <NomeArquivo>7c58b0ed1154c45aa89b8dcb7478c36a.jpg?1530034435</NomeArquivo>
        <URLArquivo>
          http://static.nidoimovel.com.br/996a7fa078cc36c46d02f9af3bef918b/imovel/BM/BM8445/7c58b0ed1154c45aa89b8dcb7478c36a.jpg?1530034435
        </URLArquivo>
        <Descricao/>
        <Titulo/>
        <Principal>1</Principal>
      </Foto>
    </Fotos>
  </Imovel>
</Imoveis>
```

Os **requisitos mínimos** para um arquivo de importação válido são:

1. Estar codificado em **UTF-8**.
1. Informar a lista dos imóveis com os campos mínimos (listados no tópico _Como funciona a importação do XML para a plataforma RuaDois?_).
1. Informar se será utilizado o padrão númerico brasileiro (Ex.: 2.000,00) ou americano (Ex.: 2,000.00) nos campos referentes a valor.

Abaixo, segue a lista de todas as informações que a Plataforma RuaDois recebe:

| Campo	                    | Obrigatoriedade	| Tipo de dado	| Exemplo	            | Observações |
|:-------------------------:|:-----------------:|:-------------:|:-------------------------:|:-----------:|
| Código	            | **obrigatório**   | String        | R20001	            | Deve ser único para referenciar o imóvel no CRM                   |
| Tipo do imóvel	    | **obrigatório**   | String        | Apartamento               |                                                                   |
| Subtipo do imóvel	    | opcional          | String        | Loft                      |                                                                   |
| Categoria do imóvel	    | opcional          | String        | Duplex                    |                                                                   |
| CEP	                    | **obrigatório**   | String        | 72.911-190	            | Não precisa vir formatado                                         |
| Bairro	            | **obrigatório**   | String        | Vila Verde                |                                                                   |
| Cidade	            | **obrigatório**   | String        | Brasília                  |                                                                   |
| UF	                    | **obrigatório**   | String        | DF                        |                                                                   |
| Logradouro	            | **obrigatório**   | String        | QR 433                    |                                                                   |
| Rua	                    | opcional          | String        | Rua D. Pedro              |                                                                   |
| Número	            | **obrigatório**   | Inteiro       | 48                        |                                                                   |
| Complemento	            | opcional          | String        | Bloco B Ap 203            |                                                                   |
| Condomínio	            | opcional          | String        | Condomínio Vila Bela      |                                                                   |
| Valor do aluguel	    | **obrigatório**   | Inteiro       | 900	                    | O sistema arredonda o valor informado.                            |
| Valor do IPTU	            | **obrigatório**   | Inteiro       | 38	                    | O sistema arredonda o valor informado.                            |
| Taxa de Administração	    | opcional          | Inteiro       | 10	                    | O sistema arredonda o valor informado.                            |
| Preço de Venda	    | opcional          | Inteiro       | 250.000,00	            | O sistema arredonda o valor informado.                            |
| Link para foto	    | opcional          | RL            |                           |                                                                   |
| Descrição da foto	    | opcional          | Texto         |                           |                                                                   |
| Foto Principal	    | opcional          | Boleano       | 1                         |                                                                   |
| Área total	            | opcional          | Inteiro       | 150	                    | Deve ser informado em m2. O sistema arredonda o valor informado.  |
| Área privada	            | opcional          | Inteiro       | 100	                    | Deve ser informado em m2. O sistema arredonda o valor informado.  |
| Área construída	    | opcional          | Inteiro       | 100	                    | Deve ser informado em m2. O sistema arredonda o valor informado.  |
| Área pública	            | opcional          | Inteiro       | 50	                    | Deve ser informado em m2. O sistema arredonda o valor informado.  |
| Descrição	            | opcional          | Texto         |                           |                                                                   |
| Quantidade de salas	    | opcional          | Inteiro       | 2                         |                                                                   |
| Quantidade de quartos	    | opcional          | Inteiro       | 3                         |                                                                   |
| Quantidade de suítes	    | opcional          | Inteiro       | 1                         |                                                                   |
| Quantidade de banheiros   | opcional          | Inteiro       | 2                         |                                                                   |
| Quantidade de garagens    | opcional          | Inteiro       | 1                         |                                                                   |
| Psicina	            | opcional          | Boleano       | 0                         |                                                                   |
| Ar condicionado	    | opcional          | Boleano       | 0                         |                                                                   |
| Mobiliado	            | opcional          | Boleano       | 0                         |                                                                   |
| Fogão	                    | opcional          | Boleano       | 0                         |                                                                   |
| Geladeira	            | opcional          | Boleano       | 0                         |                                                                   |
| Permite Animais	    | opcional          | Boleano       | 1                         |                                                                   |

A seguir, algumas ressalvas e observações:

### Tipos de imóveis
O tipo do imóvel pode ser informado em um único campo passando como no exemplo:

```xml
<TipoImovel>Apartamento Duplex</TipoImovel>
```
Mas, se informado de forma categorizada como é mostrado abaixo, a precisão na classificação do imóvel será melhor.

```xml
<TipoImovel>Apartamento</TipoImovel>
<Subtipo>Loft</Subtipo>
<Categoria>Duplex</Categoria>
```

### Fotos do imóvel
Esperamos receber um array com as várias imagens referentes ao imóvel. O essencial é que seja informado a URL para download da imagem. Dessa forma, a seguinte estrutura de XML é válida:

```xml
<Imovel>
  <Fotos>
    <Foto>https://www.imobiliaria.com.br/imovel/abc/foto1.jpg</Foto>
    <Foto>https://www.imobiliaria.com.br/imovel/abc/foto2.jpg</Foto>
    <Foto>https://www.imobiliaria.com.br/imovel/abc/foto3.jpg</Foto>
    <Foto>https://www.imobiliaria.com.br/imovel/abc/foto4.jpg</Foto>
  </Fotos>
</Imovel>
```

Contudo, a segunda estrutura apresentada abaixo, nos dá muito mais informações, enriquecendo o sistema:

```xml
<Imovel>
  <Fotos>
    <Foto>
      <URL>https://www.imobiliaria.com.br/imovel/abc/foto1.jpg</URL>
      <Descricao>Entrada principal</Descricao>
      <Principal>1</Principal>
    </Foto>
    <Foto>
      <URL>https://www.imobiliaria.com.br/imovel/abc/foto2.jpg</URL>
      <Descricao>Suíte</Descricao>
      <Principal>0</Principal>
    </Foto>
    <Foto>
      <URL>https://www.imobiliaria.com.br/imovel/abc/foto3.jpg</URL>
      <Descricao>Sala de estar</Descricao>
      <Principal>0</Principal>
    </Foto>
  </Fotos>
</Imovel>
```
---

## CRMs que já estão integrados

- Casa Soft
- Hal Sistemas
- inGaia
- Kurole
- Netimóveis
- Nido
- Union
- Universal Software
- Vista
